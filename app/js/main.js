$(window).bind("load", function() {
    doLoading()
    $('body').addClass('loaded')
});


function doLoading(){
    $('.js_loaded .loaded__info__line').addClass('scaleX') // time 0.75s

    setTimeout(() => {
        $('.js_loaded .loaded__info').addClass('hideLoadedInfo') // time 0.25s
    }, 750);

    setTimeout(() => {
        $('.js_loaded .loaded__info__line').addClass('rotate') // time 0.5s
    }, 1000);

    setTimeout(() => {
        $('.js_loaded .loaded__inner__layout--1').addClass('move') // time 0.75s
        $('.js_loaded .loaded__inner__layout--2').addClass('move') // time 0.75s
        $('.js_loaded .loaded__info__line').addClass('hide')
    }, 1500);

    setTimeout(() => {
        $('.js_loaded').addClass('hidex');
    },2250)
}



$(function () {

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'outside',
    });



    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });




    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu__view').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu__overlay').toggleClass('active')
    });



    $(".js__animate__left_1").each(function () {
        el = $(this)[0];
        delayTime = 0.15;
        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__right_1").each(function () {
        el = $(this)[0];
        delayTime = 0.3;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__left").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__right").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__top").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 120%'},
            y: 150,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__bottom").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 120%'},
            y: -150,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__opacity").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })




    $(".js__animate__top_0").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            y: 30,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__scale").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing')[0],start: 'top 50%'},
            scale: 0,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__top_dot").each(function () {
        let el = $(this)[0];
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            y: -50,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })



    $(".js__animate__top_line").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $.fn.isInViewport = function () {
        let elementTop = $(this).offset().top;
        let elementBottom = elementTop + $(this).outerHeight();

        let viewportTop = $(window).scrollTop();
        let viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };


    let animateStartCount = false;

    $(window).on('resize scroll', function () {
        if ($('.js_count_animate').isInViewport() && !animateStartCount) {
            animateStartCount = true;
            $('.js_count_animate').each(function () {
                var $this = $(this),
                    countTo = $this.attr('data-count');

                $({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },
                    {
                        duration: 2500,
                        easing: 'linear',
                        step: function () {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $this.text(this.countNum);
                        }

                    });

            });
        }
    });

});